/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to sagas/index.js
*  - This template uses the api declared in sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/

import { call, put, select, take } from 'redux-saga/effects'
import SingleChatActions from '../Redux/SingleChatRedux'
import {SingleChatSelectors } from "../Redux/SingleChatRedux";
import { CoreSelectors } from "../Redux/CoreRedux";
import {END, eventChannel} from "redux-saga";
import SendBird from "sendbird";
import {GROUP_TABLE, MESSAGES_TABLE, DEFAULT_USER_PROFILE_PHOTO, APP_ID} from "../Constants";
import {selectGroupList, insertIntoTableQueryGenerator,selectQueryGenerator } from "../Databases/InitDB";
import GroupListActions from "../Redux/GroupListRedux";
import { CoreSelectorsAllData } from "../Redux/CoreRedux";
import CoreActions from "../Redux/CoreRedux";

export function * fetchChannelMessagesSaga(action) {

  const db = yield call(selectGroupList);//dis actually opens a db connection, please ignore the wrong method naming
  const channel = eventChannel(emitter => {
    db.transaction(tx => {
      emitter({tx});
      emitter(END);
    }, (err)=>{
      emitter({err});
      emitter(END);
    }, ()=>{});

    // Return an unsubscribe method
    return () => {
    };
  });

  while (true){
    const {tx} = yield take(channel);

    if(tx){
      let stmt = `SELECT * from ${MESSAGES_TABLE} WHERE group_identifier = '${action.otherPartyId}' ORDER by id desc`;

      const transactionChannel = eventChannel(emitter => {
        tx.executeSql(stmt, [], (tx, results)=>{emitter({results}); emitter(END)}, (err)=>{emitter({err}); emitter(END)});

        // Return an unsubscribe method
        return () => {

        };
      });
      const { results, err } = yield take(transactionChannel);
      if (err){
        //I dunno
      } else {
        let messages = [];

        let length = results.rows.length;

        for (let w = 0; w < length; w++){
          let dbMessage = (results.rows.item(w));
          let message = {};
          message._id = dbMessage.id;
          message.text = dbMessage.message_body;
          message.createdAt = new Date(dbMessage.created_at);
          message.user = JSON.parse(dbMessage.user);
          message.sent = dbMessage.is_sent == 1 ? true : false;
          //message.received = true;
          messages.push(message);
        }
        yield put(SingleChatActions.singleChatSetMessages(messages))
      }
    }


  }


}

export function * sendMessageSaga(action) {

  //insert it into last sent
  const db = yield call(selectGroupList);//dis actually opens a db connection, please ignore the wrong method naming
  const channel = eventChannel(emitter => {
    db.transaction(tx => {
      emitter(tx);
      emitter(END);
    }, (err) => {
      emitter({err});
      emitter(END);
    }, () => {
    });

    // Return an unsubscribe method
    return () => {
    };
  });

  while (true) {
    const response = yield take(channel);

    let updateStmt = `UPDATE ${GROUP_TABLE} SET last_message = '${action.message}', updated_at = ${new Date().getTime()} WHERE user_id = '${action.userId}'`;
    const transactionChannel = eventChannel(emitter => {
      response.executeSql(updateStmt, [], (tx, results) => {

        let dateNow = new Date();
        let user = {
          _id: action.fromUserId,
          name: '',
          avatar: DEFAULT_USER_PROFILE_PHOTO
        };
        user = JSON.stringify(user);
        let insertMessageStmt = insertIntoTableQueryGenerator(MESSAGES_TABLE, {
          group_identifier: action.userId,
          message_body: action.message,
          created_at: dateNow.getTime(),
          updated_at: dateNow.getTime(),
          user
        });
        response.executeSql(insertMessageStmt, [], () => {
          emitter({results});
          emitter(END)
        }, (err) => {
          emitter({err});
          emitter(END);
        });

      }, (err) => {
        emitter({err});
        emitter(END)
      });

      // Return an unsubscribe method
      return () => {

      };
    });
    let {results, err } = yield take(transactionChannel);

    yield put(SingleChatActions.singleChatFetchChannelMessages(action.userId))

    let appCoreData = yield select(CoreSelectorsAllData.getData);
    let {isConnected} = appCoreData.isConnected;
    console.log(isConnected);
    let {sendBirdInstance} = appCoreData.sendBirdInstance;
    if (sendBirdInstance == null) {
      sendBirdInstance = new SendBird({appId: APP_ID});
      yield put(CoreActions.coreSendBirdInstance(sendBirdInstance))
    }

    if (sendBirdInstance && isConnected == 1) {

      const channel = eventChannel(emitter => {
        sendBirdInstance.GroupChannel.createChannelWithUserIds([action.userId], true, (createdChannel, error) => {
          if (error) {
            emitter({error});
            emitter(END)
          } else {
            emitter({createdChannel});
            emitter(END);
          }
        });
        // Return an unsubscribe method
        return () => {
        };
      });

      const {createdChannel, error} = yield take(channel)


      if (createdChannel) {
        createdChannel.sendUserMessage(action.message, (message, error) => {
          if (error) {
            console.error(error);
            return;
          }
          console.log(message);
        });
      }

    }
  }
}



export function * openSendBirdGroupChannelSaga (action) {
  const { data } = action;

  let appCoreData = yield select(CoreSelectorsAllData.getData);
  let {sendBirdInstance} = appCoreData.sendBirdInstance;
  if (sendBirdInstance == null) {
    sendBirdInstance = new SendBird({ appId: APP_ID });
    yield put(CoreActions.coreSendBirdInstance(sendBirdInstance))
  }

  if (sendBirdInstance && sendBirdInstance.getCurrentUserId()) {
    const channel = eventChannel(emitter => {
      sendBirdInstance.GroupChannel.createChannelWithUserIds(data, true, (createdChannel, error) => {
        if (error) {
          emitter({error});
          emitter(END)
        }else{
          emitter({createdChannel});
          emitter(END);
        }
      });
      // Return an unsubscribe method
      return () => {
      };
    });

    const { createdChannel, error } = yield take(channel)
    if(createdChannel){
      yield put(SingleChatActions.singleChatSuccessCreateGroupChannel(createdChannel));

      //Update channel Url

      //insert it into last sent
      const db = yield call(selectGroupList);//dis actually opens a db connection, please ignore the wrong method naming
      const channel = eventChannel(emitter => {
        db.transaction(tx => {
          emitter(tx);
          emitter(END);
        }, (err)=>{
          emitter({err});
          emitter(END);
        }, ()=>{});

        // Return an unsubscribe method
        return () => {
        };
      });

      while (true){
        const response = yield take(channel);
        let updateStmt = `UPDATE ${GROUP_TABLE} SET group_url = '${createdChannel.url}' WHERE user_id = '${data[0]}'`;

        const transactionChannel = eventChannel(emitter => {
          response.executeSql(updateStmt, [], (tx, results)=>{

            emitter(results);
            emitter(END)
            //emitter(results.rows);
          }, (err)=>{
            emitter({err});
            emitter(END);
          });

          // Return an unsubscribe method
          return () => {

          };
        });
        let updateTableResponse = yield take(transactionChannel);

      }

    }
  }
}

// let startChat = () => {
//
//
//   var userIds = ['+2348160831611'];
//   sb.GroupChannel.createChannelWithUserIds(userIds, true, (createdChannel, error) => {
//     if (error) {
//       console.error(error);
//       return;
//     }
//   });
// }
