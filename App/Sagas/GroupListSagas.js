/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to sagas/index.js
*  - This template uses the api declared in sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/

import {call, put, select, take} from 'redux-saga/effects'
import { eventChannel, END } from 'redux-saga';
import GroupListActions from '../Redux/GroupListRedux'
import HomeActions from '../Redux/HomeRedux'
import SendBird from "sendbird";
import {APP_ID, GROUP_TABLE} from "../Constants";
import { selectGroupList, selectQueryGenerator } from "../Databases/InitDB";
import {CoreSelectorsAllData} from "../Redux/CoreRedux";

import CoreActions from "../Redux/CoreRedux";

// export function * getGroupList (action) {
//   const db = yield call(selectGroupList);
//
//   const channel = eventChannel(emitter => {
//     db.transaction(tx => {
//       emitter(tx) ;
//     }, (err)=>{
//       emitter(END);
//     }, ()=>{});
//
//     // Return an unsubscribe method
//     return () => {
//     };
//   });
//
//   while (true){
//     const res = yield take(channel);
//     let stmt = selectQueryGenerator(GROUP_TABLE, [], [], ['updated_at', 'desc']);
//
//     const transactionChannel = eventChannel(emitter => {
//       res.executeSql(stmt, [], (tx, results)=>{
//         let groups = [];
//         for(let q = 0; q < results.rows.length; q ++){
//           groups.push(results.rows.item(q));
//         }
//         emitter(groups);
//         emitter(END)
//         //emitter(results.rows);
//       }, ()=>emitter(END));
//
//       // Return an unsubscribe method
//       return () => {
//
//       };
//     });
//      const groupListRows = yield take(transactionChannel);
//      console.log(groupListRows)
//      yield put(HomeActions.homeSuccessGroupList(groupListRows));
//      break;
//   }
//
// }

export function * getGroupList (action) {
  let appCoreData = yield select(CoreSelectorsAllData.getData);
  let {sendBirdInstance} = appCoreData.sendBirdInstance;


  if(sendBirdInstance == null){
    sendBirdInstance = new SendBird({ appId: APP_ID });
    yield put(CoreActions.coreSendBirdInstance(sendBirdInstance))
  }

  if(sendBirdInstance && sendBirdInstance.getCurrentUserId()){
      const channel = eventChannel(emitter => {
        let channelListQuery = sendBirdInstance.GroupChannel.createMyGroupChannelListQuery();
        channelListQuery.limit = 20;
        if (channelListQuery.hasNext) {
          channelListQuery.next((channelList, error) => {
            if (error) {
              emitter({error})
              emitter(END)
              return;
            }
            emitter({channelList})
            emitter(END)
            console.log(channelList);
          });
        }

        // Return an unsubscribe method
        return () => {
        };
      });

      while (true){
        let {channelList,error } = yield take(channel);
        if(error){
          alert(error.message)
          break;
        }else{
          yield put(HomeActions.homeSuccessGroupList(channelList));
        }
      }
  }

}


