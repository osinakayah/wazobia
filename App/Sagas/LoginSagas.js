import { put, call } from 'redux-saga/effects'
import LoginActions from '../Redux/LoginRedux'
import {sendSmsVerification} from "../Services/AuthApi";

// attempts to login
export function * login (data) {
  const smsVerificationResponse = yield call(sendSmsVerification, data.phoneNumber)
  if (true) {
    yield put(LoginActions.loginSuccess(123456))
  } else {
    yield put(LoginActions.loginFailure('Oops, an error occured, please try again'))
  }
}
