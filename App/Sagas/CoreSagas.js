/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to sagas/index.js
*  - This template uses the api declared in sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/

import {call, put, take, select} from 'redux-saga/effects'
import CoreActions from '../Redux/CoreRedux'
import {END, eventChannel} from "redux-saga";
import {APP_ID} from "../Constants";
import SendBird from "sendbird";
import PhoneCodeVerificationActions from "../Redux/PhoneCodeVerificationRedux";
import {AsyncStorage, Platform} from "react-native";
import { CoreSelectorsAllData } from "../Redux/CoreRedux";
import {setValueOnFirebaseDB} from "../FirebaseWorkers/FirebaseDB";
import GroupListActions from "../Redux/GroupListRedux";

export function * connectToSdk (action) {
  const { data } = action

  yield put(CoreActions.coreLoggedInUserId(data))
  yield put(CoreActions.coreConnectToChatSdkFailure(0))

  let appCoreData = yield select(CoreSelectorsAllData.getData);
  let {sendBirdInstance} = appCoreData.sendBirdInstance;
  if(sendBirdInstance == null){
    sendBirdInstance = new SendBird({ appId: APP_ID });
    yield put(CoreActions.coreSendBirdInstance(sendBirdInstance))
  }

  if(sendBirdInstance){
    const channel = eventChannel(emitter => {

      sendBirdInstance.connect(data, (user, error)=>{
        if(error){
          emitter({ error });
        }else {
          emitter({ user });
        }
        emitter(END)
      })
      // Return an unsubscribe method
      return () => {
      };
    });


    while (true){
      const { error, user } = yield take(channel);
      if(error){
        yield put(CoreActions.coreConnectToChatSdkFailure(-1))
        yield put(PhoneCodeVerificationActions.phoneCodeVerificationFailure())
        alert(error.message)
      } else {
        yield put(CoreActions.coreConnectToChatSdkSuccess(1))
        yield put(PhoneCodeVerificationActions.phoneCodeVerificationSuccess(true))
        yield put(GroupListActions.groupListRequest())
        AsyncStorage.multiSet([
          ['user_is_validated', "1"],
          ['user_phone_number', data],
        ], ()=>{})

        AsyncStorage.multiGet(['user_fcm_token']).then((data)=>{
          if(data[0][1]){
            if(Platform.OS === 'ios'){
              sendBirdInstance.registerAPNSPushTokenForCurrentUser(data[0][1], (result, error)=>{
                console.log("registerAPNSPushTokenForCurrentUser");
                console.log(result);
              });
            }else{

              sendBirdInstance.registerGCMPushTokenForCurrentUser(data[0][1], (result, error) => {
                console.log("registerAPNSPushTokenForCurrentUser");
                console.log(result);
              });
            }

          }
        });

      }



    }

  }

}

export function * fcmTokenRefreshed(action){
  const { fcmToken } = action;
  AsyncStorage.multiSet([
    ['user_fcm_token', fcmToken]
  ], ()=>{})

  let appCoreData = yield select(CoreSelectorsAllData.getData);
  let {sendBirdInstance} = appCoreData.sendBirdInstance;
  if(sendBirdInstance == null){
    sendBirdInstance = new SendBird({ appId: APP_ID });
    yield put(CoreActions.coreSendBirdInstance(sendBirdInstance))
  }

  if(sendBirdInstance){
    if(Platform.OS === 'ios'){
      sendBirdInstance.registerAPNSPushTokenForCurrentUser(data[0][1], (result, error)=>{
        console.log("registerAPNSPushTokenForCurrentUser");
        console.log(result);
      });
    }else{

      sendBirdInstance.registerGCMPushTokenForCurrentUser(data[0][1], (result, error) => {
        console.log("registerAPNSPushTokenForCurrentUser");
        console.log(result);
      });
    }
  }
  if(appCoreData.loggedInUserId != 0){
    setValueOnFirebaseDB('users', appCoreData.loggedInUserId, {fcm_token: fcmToken});
  }
}
