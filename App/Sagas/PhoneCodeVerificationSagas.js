/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to sagas/index.js
*  - This template uses the api declared in sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/
import {AsyncStorage, Platform} from 'react-native'
import { call, put, select, take } from 'redux-saga/effects'
import PhoneCodeVerificationActions from '../Redux/PhoneCodeVerificationRedux'
import {createUser} from "../Services/UserApi";
import { DEFAULT_USER_PROFILE_PHOTO } from "../Constants";
import SendBird from 'sendbird'
import { CoreSelectorsAllData } from "../Redux/CoreRedux";
import {END, eventChannel} from "redux-saga";
import CoreActions from "../Redux/CoreRedux";

export function * registerUserOnSendBird (action) {
  const {data} = action;
  yield put(CoreActions.coreConnectToChatSdk(data))
}
