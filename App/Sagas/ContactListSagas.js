/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to sagas/index.js
*  - This template uses the api declared in sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/

import { call, put, take } from 'redux-saga/effects'
import ContactListActions from '../Redux/ContactListRedux'
import {END, eventChannel} from "redux-saga";

export function * getContactList (action) {

  let Contacts = require('react-native-contacts');

  const channel = eventChannel(emitter => {
    Contacts.checkPermission((err, permission) => {

      if(permission === 'authorized'){
        Contacts.getAllWithoutPhotos((err, contacts)=>{
          emitter({contacts: parseContacts(contacts)})
        })
      }
      else if(permission === 'denied' || permission == 'undefined'){
        Contacts.requestPermission( (err, permission) => {
          if(permission === 'authorized'){
            Contacts.getAllWithoutPhotos((err, contacts)=>{
              emitter({contacts: parseContacts(contacts)})
            })
          }else{
            alert('Permission to read contacts denied');
            emitter(END)
          }
        })
      }
    });

    return () => {
    };
  });

  while (true){
    const res = yield take(channel);
    yield put(ContactListActions.contactListSuccess(res))
    if (!res) {
      break;
    }
  }

}


function isValidPhoneNumber(phone){
  if(phone.length <= 10 || phone.length >=14){
    return false;
  }
  //Just nigerian only
  let arr = phone.split("");
  arr = arr.reverse();
  phone = arr.join("");
  phone = phone.substring(0, 10);

  if(phone.length === 10){
    return true;
  }
  return false

}
function parseContacts(contacts){
  let contact = {};
  let myContacts = [];

  for (let val of contacts) {
    let phoneNumbers = val.phoneNumbers;

    for(let j of phoneNumbers){
      if(isValidPhoneNumber(j.number) && !validateEmail(j.number)){
        contact.givenName = val.givenName;
        contact.number = j.number.replace(/\s+/g, '');

        if(!isInContacts(myContacts, contact.number)){
          myContacts.push(contact);
          contact = {};
        }

      }
    }
  }
  return myContacts;
}
function isInContacts(myContacts, number){
  for(var i = 0; i < myContacts.length; i++) {
    if (myContacts[i].number == number) {
      return true
    }
  }
  return false;
}
function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}
