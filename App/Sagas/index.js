import { takeLatest, all, takeEvery } from 'redux-saga/effects'
import API from '../Services/Api'
import FixtureAPI from '../Services/FixtureApi'
import DebugConfig from '../Config/DebugConfig'

/* ------------- Types ------------- */

import { StartupTypes } from '../Redux/StartupRedux'
import { GithubTypes } from '../Redux/GithubRedux'
import { LoginTypes } from '../Redux/LoginRedux'
import { PhoneCodeVerificationTypes } from "../Redux/PhoneCodeVerificationRedux";
import { ContactListTypes } from "../Redux/ContactListRedux";
import { GroupListTypes } from "../Redux/GroupListRedux";
import { CoreTypes } from "../Redux/CoreRedux";
import { SingleChatTypes } from "../Redux/SingleChatRedux";

/* ------------- Sagas ------------- */

import { startup } from './StartupSagas'
import { login } from './LoginSagas'
import { getUserAvatar } from './GithubSagas'
import { registerUserOnSendBird } from "./PhoneCodeVerificationSagas";
import { getContactList } from "./ContactListSagas";
import { getGroupList } from "./GroupListSagas";
import { connectToSdk, fcmTokenRefreshed } from "./CoreSagas";
import { openSendBirdGroupChannelSaga, sendMessageSaga, fetchChannelMessagesSaga } from "./SingleChatSagas";

/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const api = DebugConfig.useFixtures ? FixtureAPI : API.create()

/* ------------- Connect Types To Sagas ------------- */

export default function * root () {
  yield all([
    // some sagas only receive an action
    takeLatest(StartupTypes.STARTUP, startup),
    takeLatest(LoginTypes.LOGIN_REQUEST, login),
    takeLatest(ContactListTypes.CONTACT_LIST_REQUEST, getContactList),

    takeLatest(PhoneCodeVerificationTypes.PHONE_CODE_VERIFICATION_REQUEST, registerUserOnSendBird),
    takeLatest(GroupListTypes.GROUP_LIST_REQUEST, getGroupList),

    takeLatest(CoreTypes.CORE_CONNECT_TO_CHAT_SDK, connectToSdk),

    // some sagas receive extra parameters in addition to an action
    takeLatest(GithubTypes.USER_REQUEST, getUserAvatar, api),
    takeEvery(SingleChatTypes.SINGLE_CHAT_REQUEST_CREATE_GROUP_CHANNEL, openSendBirdGroupChannelSaga),
    takeEvery(SingleChatTypes.SINGLE_CHAT_SEND_MESSAGE, sendMessageSaga),
    takeEvery(SingleChatTypes.SINGLE_CHAT_FETCH_CHANNEL_MESSAGES, fetchChannelMessagesSaga),
    takeLatest(CoreTypes.CORE_FCM_TOKEN_REFRESHED, fcmTokenRefreshed)
  ])
}
