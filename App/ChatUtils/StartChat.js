import { openDatabaseAndPerformOperation, selectQueryGenerator, insertIntoTableQueryGenerator } from "../Databases/InitDB";
import { GROUP_TABLE } from "../Constants";

export function startNewChat(group_name, user_id, group_url, display_url, last_message, unread_message_count, is_sync) {
  /*
  Insert into db
  Start new group
  //
   */
  let updated_at = new Date().getTime();
  let  stmt = insertIntoTableQueryGenerator(GROUP_TABLE, {group_name, user_id, group_url, display_url, last_message, unread_message_count, is_sync, updated_at});


  openDatabaseAndPerformOperation(stmt, []);
}
