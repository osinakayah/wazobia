import React, { Component } from 'react'
import {AsyncStorage, BackHandler} from 'react-native'
import { Content, Container, Header, Left, Right, Body, Button, Text, Title, Icon, Footer, FooterTab } from 'native-base'
import { connect } from 'react-redux'
import {Colors, Images} from '../Themes'
import AppIntroSlider from 'react-native-app-intro-slider';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/IntroScreenStyle'
import SplashScreen from "react-native-splash-screen";
import {NavigationActions} from "react-navigation";
const slides = [
  {
    key: 'somethun',
    title: 'Friends',
    text: 'Chat with friends',
    image: Images.chat_with_friends,
    imageStyle: styles.image,
    backgroundColor: '#59b2ab',
  },
  {
    key: 'somethun-dos',
    title: 'Security Agencies',
    text: 'Chat with Emergency Agencies',
    image: Images.chat_with_secuirty,
    imageStyle: styles.image,
    backgroundColor: '#febe29',
  },
  {
    key: 'somethun1',
    title: 'News hub',
    text: 'Wazobia Information Hub',
    image: Images.chat_news_hub,
    imageStyle: styles.image,
    backgroundColor: '#22bcb5',
  }
];
class IntroScreen extends Component {
  componentDidMount(){
    AsyncStorage.multiGet(['user_is_validated']).then(data => {
      if(data[0][1] && data[0][1] == "1"){

        this.props.navigation.dispatch(NavigationActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({ routeName: 'NavigationDrawer' })
          ]
        }))
      }
      SplashScreen.hide();
    });
  }
  _onDone = () => {
    this.props.navigation.dispatch(NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'Login' })
      ]
    }))
  }
  render () {
    return (
      <Container>
        <Header style={{backgroundColor: Colors.primary, display: 'none'}} androidStatusBarColor={ Colors.primary_dark}>
          <Left></Left>
          <Body>
          <Title>Wazobia Chat</Title>
          </Body>
        </Header>
        <Content>
          <AppIntroSlider
            slides={slides}
            onDone={this._onDone}
          />
        </Content>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(IntroScreen)
