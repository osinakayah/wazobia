import React, { Component } from 'react'
import { StyleSheet, AppState, Platform  } from 'react-native'
import { Content, Container, Header, Left, Right, Body, Button, Text, Title, Icon, Tab, Tabs  } from 'native-base'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'
// Styles
import styles from './Styles/HomeScreenStyle'
import FriendsList from "../Components/FriendsList";
import NewsList from "../Components/NewsList";
import {registerAppListener} from "../FirebaseWorkers/Listeners";
import GroupListActions from "../Redux/GroupListRedux";
import {Colors} from "../Themes";
import SplashScreen from "react-native-splash-screen";
import CoreActions from "../Redux/CoreRedux";

class HomeScreen extends Component {
  componentDidMount(){
    SplashScreen.hide();
    this.props.getGroupList();
    registerAppListener(this.props.navigation);
  }
  retryConnection = () => {
    this.props.connectToChatSdk(this.props.loggedInUserId);
  }
  openSingleChat = (channelUrl, userNumber, chatTitle) => {
    this.props.navigation.navigate('SingleChatScreen', {channelUrl: channelUrl, userIds: userNumber, chatTitle});
    //this.props.navigation.navigate('SingleChatScreen', {channelUrl: '', userIds: userNumber});
  }
  openContactsList = () =>{
    this.props.navigation.navigate('ContactListScreen');
  }
  render () {

    return (
      <Container>
        <Header hasTabs style={{backgroundColor: Colors.primary}} androidStatusBarColor={Colors.primary_dark}>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("DrawerOpen")}
            >
              <Icon name="menu" />
            </Button>
          </Left>
          <Body>
            <Title>Wazobia</Title>
          </Body>
          <Right />
        </Header>
        <Tabs initialPage={0}>
          <Tab heading="Chat" tabStyle={{backgroundColor: Colors.primary}} activeTabStyle={{backgroundColor: Colors.primary}}>
            <FriendsList retryConnection={this.retryConnection} isConnected={this.props.isConnected} groupList={this.props.homeStore.groupList} openContactsList={this.openContactsList} openSingleChat={this.openSingleChat} />
          </Tab>
          <Tab heading="Hub" tabStyle={{backgroundColor: Colors.primary}} activeTabStyle={{backgroundColor: Colors.primary}}>
            <NewsList />
          </Tab>
        </Tabs>

      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    homeStore: state.homeStore,
    isConnected:state.appCore.isConnected,
    loggedInUserId: state.appCore.loggedInUserId
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getGroupList: ()=> dispatch(GroupListActions.groupListRequest()),
    connectToChatSdk: loginId=> dispatch(CoreActions.coreConnectToChatSdk(loginId)),

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen)
