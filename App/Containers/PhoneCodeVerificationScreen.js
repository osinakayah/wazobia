import React, { Component } from 'react'
import {AsyncStorage, NetInfo, Platform} from 'react-native'
import { Content, Container, Header, Left, Right, Body, Button, Text, Title, Icon, List, ListItem, Toast } from 'native-base'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'
import {NavigationActions} from "react-navigation";
import PhoneCodeVerificationActions from '../Redux/PhoneCodeVerificationRedux'
import {Colors, Fonts} from "../Themes";
// Styles
import styles from './Styles/PhoneCodeVerificationScreenStyle'
import SpinnerOverlay from 'react-native-loading-spinner-overlay';
import CodeInput from 'react-native-confirmation-code-input';
import {DEFAULT_USER_PROFILE_PHOTO} from "../Constants";
import SendBird from "sendbird";
var sb = null;

class PhoneCodeVerificationScreen extends Component {
  constructor(props){
    super(props);
    //this._onPressConnect = this._onPressConnect.bind(this);
  }
  componentWillReceiveProps(newProps){
    if(newProps.phoneCodeVerificationTypes.payload){
      this.props.navigation.dispatch(NavigationActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({ routeName: 'NavigationDrawer' })
        ]
      }));
    }
  }
  _onFinishCheckingCode(isValid){
    if(isValid){
      NetInfo.isConnected.fetch().then(isConnected => {

        if(isConnected){
          //this._onPressConnect();
          this.props.registerOnSendBird(this.props.navigation.state.params.phoneNumber);
        }else{
          Toast.show({
            text: 'Check your internet connection',
            position: 'top',
            type: 'danger',
            duration: 3000
          })
        }
      });
      //this.props.registerOnSendBird({phoneNumber: this.props.navigation.state.params.phoneNumber});

      //this.props.navigation.navigate("NavigationDrawer");
    }else{
      Toast.show({
        text: 'Invalid validation code',
        position: 'top',
        type: 'danger',
        duration: 3000
      })
    }
  }
  render () {
    const {goBack} = this.props.navigation;
    const {params} = this.props.navigation.state;
    return (
      <Container>
        <Header style={{backgroundColor: Colors.primary}} androidStatusBarColor={ Colors.primary_dark}>
          <Left>
            <Button transparent onPress={() => {
              goBack()
            }}>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
          <Title style={{fontFamily: Fonts.type.base}}>Verification</Title>
          </Body>
        </Header>

        <Content padder>
          <SpinnerOverlay visible={this.props.phoneCodeVerificationTypes.fetching} textContent={"Loading..."} textStyle={{color: '#FFF'}} />
          <Text style={{textAlign: 'center', marginTop:15, marginBottom: 15, fontFamily: Fonts.type.bold}}>Verify {this.props.navigation.state.params.phoneNumber}</Text>
          <Text style={{textAlign: 'center', marginTop:15, marginBottom: 15, fontFamily: Fonts.type.base}}>Waiting to detect an SMS sent to {this.props.navigation.state.params.phoneNumber}</Text>
          <CodeInput
            codeLength={6}
            keyboardType="numeric"
            className='border-l-r'
            ref="codeInputRef2"
            compareWithCode={params.verificationCode}
            activeColor='rgba(103,58,183, 1)'
            inactiveColor='rgba(103,58,183, 1)'
            autoFocus={false}
            inputPosition='center'
            size={50}
            onFulfill={(isValid) => this._onFinishCheckingCode(isValid,params.verificationCode)}
            containerStyle={{ marginTop: 30 }}
            codeInputStyle={{ borderWidth: 1.5 }}
          />
          <Text style={{textAlign: 'center', marginTop: 3, marginBottom: 15, fontFamily: Fonts.type.base, fontSize: 12}}>Enter 6 digit code</Text>
          <List style={{marginTop: 15, marginBottom: 15}}>
            <ListItem icon>
              <Left>
                <Icon name="md-chatboxes" />
              </Left>
              <Body>
                <Text>Resend SMS</Text>
              </Body>
              <Right>
                <Text>01:20</Text>
              </Right>
            </ListItem>
            <ListItem icon>
              <Left>
                <Icon name="arrow-back" />
              </Left>
              <Body>
              <Text>Wrong Number</Text>
              </Body>
              <Right>
                <Text>01:20</Text>
              </Right>
            </ListItem>
          </List>
        </Content>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    phoneCodeVerificationTypes: state.phoneCodeVerificationTypes
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    registerOnSendBird: data => dispatch(PhoneCodeVerificationActions.phoneCodeVerificationRequest(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PhoneCodeVerificationScreen)
