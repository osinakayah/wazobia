import React, { Component } from 'react'
import {Image, View, Platform, AppState, NetInfo} from 'react-native'
import { Content, Container, Header, Left, Right, Body, Button, Text, Title, Icon, Footer, FooterTab } from 'native-base'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'
import SingleChatActions from '../Redux/SingleChatRedux';
import { GiftedChat, Send, Bubble, MessageText } from 'react-native-gifted-chat'
import { Images, Colors , Fonts} from "../Themes";
// Styles
import styles from './Styles/SingleChatScreenStyle'
import firebaseClient from  "../FirebaseWorkers/FirebaseClient";
import SendBird from "sendbird";
import {getFCMDBReference, setValueOnFirebaseDB} from "../FirebaseWorkers/FirebaseDB";
import SplashScreen from "react-native-splash-screen";

var sb = null;
let openChannnel = null;
class SingleChatScreen extends Component {
  constructor(props){
    super(props);
    sb = SendBird.getInstance();
    this.state = {
      messages: [],
      receiverFcmToken: null,
    }


  }

  renderAccessory(props) {

    return (
      <View style={{display: 'flex', flexDirection: 'row'}}>
        <Button transparent>
          <Icon name='md-image' />
        </Button>
        <Button transparent>
          <Icon name='md-videocam' />
        </Button>
      </View>
    );
  }
  componentDidMount = ()=>{
    SplashScreen.hide();
    let firebaseDB = getFCMDBReference();
    const {params} = this.props.navigation.state;
    if(firebaseDB){
      let userRef = firebaseDB.ref('users/'+params.userIds);

      userRef.on('value', (snapshot)=>{
        if (snapshot.val()){
          this.setState({receiverFcmToken: snapshot.val().fcm_token })
        }else {
          alert("this is not a registered user on wazobiachat");
          this.props.navigation.goBack();
        }

      })
    }


    //let groupChannel = this.props.singleChatRedux.groupChannel;
    this.props.fetchMessages(params.userIds);
    //this.props.openGroupChannnel([params.userIds]);
    // if(params.channelUrl === '' && groupChannel == null){
    //   //If i have no reference when user is conn, would implement leta
    //   //Channel has not been creeated on send bird yet
    //   this.props.openGroupChannnel([params.userIds]);
    //   return;
    // }

    // if(groupChannel){
    //   openChannnel = groupChannel;
    //   let ChannelHandler = new sb.ChannelHandler();
    //
    //   ChannelHandler.onMessageReceived = (channel, message) => {
    //     console.log(message);
    //     let giftMessage = {};
    //     giftMessage._id = message.messageId;
    //     giftMessage.sent = true;
    //     giftMessage.text = message.message;
    //     giftMessage.createdAt = new Date();
    //     let user = {};
    //     user._id = message._sender.userId;
    //     user.name = message._sender.nickname;
    //     user.avatar = message._sender.profileUrl;
    //     giftMessage.user = user;
    //     this.setState(previousState => ({
    //       messages: GiftedChat.append(previousState.messages, [giftMessage]),
    //     }))
    //   };
    //   sb.addChannelHandler('Message_Received_Handler', ChannelHandler);
    // }

  }

  sendRemoteData(title, message, token) {

    let body;
    if(Platform.OS === 'android'){
      body = {
        to: token,
        data:{
          custom_notification: {
            title: title,
            body: message,
            sound: "default",
            priority: "high",
            show_in_foreground: true,
            targetScreen: 'single_chat_screen',
            senderNumber: this.props.loggedInUserId
          }
        },
        priority: 10
      }
    } else {
      body = {
        to: token,
        notification:{
          title: title,
          body: message,
          sound: "default"
        },
        data: {
          targetScreen: 'single_chat_screen',
          senderNumber: this.props.loggedInUserId
        },
        priority: 10
      }
    }
    firebaseClient.send(JSON.stringify(body), "data");
  }

  onSend(messages = []) {
    // openChannnel.sendUserMessage(messages[0].text, (message, error) => {
    //   if (error) {
    //     console.error(error);
    //     return;
    //   }
    // });
    const {params} = this.props.navigation.state;
    NetInfo.isConnected.fetch().then(isConnected => {
      if(isConnected){
        this.sendRemoteData('Wazobia', messages[0].text, this.state.receiverFcmToken);
      }
    });


    this.props.sendMessage(params.userIds, messages[0].text, this.props.loggedInUserId, params.userIds);

    // this.setState(previousState => ({
    //   messages: GiftedChat.append(previousState.messages, messages),
    // }))
    this.props.fetchMessages(params.userIds);
  }

  renderSend(props) {
    return (
      <Send
        {...props}
      >
        <View style={{marginRight: 10, marginBottom: 3}}>
          <Icon name='ios-send'/>
        </View>
      </Send>
    );
  }

  render () {
    const {params} = this.props.navigation.state;
    return (
      <Container>
        <Header style={{backgroundColor: Colors.primary}} androidStatusBarColor={Colors.primary_dark}>
          <Left>
            <Button transparent onPress={() => {
              this.props.navigation.navigate('HomeScreen');
            }}>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>{params.chatTitle}</Title>
          </Body>
          <Right />

        </Header>
        {/*<Image source={Images.background} style={styles.backgroundImage} resizeMode="stretch" />*/}

        <GiftedChat
          text={this.props.singleChatRedux.customText}
          onInputTextChanged={text => this.props.setCustomText(text)}
          renderAccessory={this.renderAccessory}
          isAnimated={true}
          renderBubble={this.renderBubble.bind(this)}
          renderSend={this.renderSend.bind(this)}
          messages={this.props.singleChatRedux.messages}
          //messages={this.state.messages}
          onSend={messages => this.onSend(messages)}
          renderMessageText={this.renderMessageText.bind(this)}
          user={{
            _id: this.props.loggedInUserId,
            name: 'Osi',
            avatar: 'http://icons.iconarchive.com/icons/diversity-avatars/avatars/512/muslim-man-icon.png'
          }}
        />

      </Container>
    )
  }
  renderBubble(props) {
    return (
      <Bubble
        {...props}
        wrapperStyle={{
          left: {
            backgroundColor: Colors.silver,
          },
          right: {
            backgroundColor: Colors.primary,
          }
        }}
      />
    );
  }

  renderMessageText(props) {
    return (
      <View>
        <MessageText
          {...props}
        />
      </View>
    );
  }



}

const mapStateToProps = (state) => {
  return {
    singleChatRedux: state.singleChatRedux,
    appCore: state.appCore.isConnected,
    loggedInUserId: state.appCore.loggedInUserId
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    openGroupChannnel: (userIds)=> dispatch(SingleChatActions.singleChatRequestCreateGroupChannel(userIds)),
    sendMessage: (userId, message, fromUserId, toUserId) => dispatch(SingleChatActions.singleChatSendMessage(userId, message, fromUserId, toUserId)),
    fetchMessages: (recipeintID)=> dispatch(SingleChatActions.singleChatFetchChannelMessages(recipeintID)),
    setCustomText: text => dispatch(SingleChatActions.singleChatSetCustomText(text))
    // enterOpenChannnel: () => {},
    // sendMessagePrivateMessage: (recipeintId)=>{}
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SingleChatScreen)
