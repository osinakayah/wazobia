import React, { Component } from "react";
import { Image } from "react-native";
import { connect } from 'react-redux'
import { List, ListItem, Text, View, Content, Container, Body, Button, Icon } from "native-base";

import styles from "./Styles/DrawerContentStyles";
import { Images, Metrics, Colors, Fonts } from "../Themes";

class DrawerContent extends Component {
	render() {
		const navigation = this.props.navigation;
		const items = this.props.items;
		return (
			<Container>
				<Content style={{ height: Metrics.screenHeight-25}}>
          <View style={{padding: 12, backgroundColor: Colors.primary}}>
            <Image source={{uri: 'http://icons.iconarchive.com/icons/diversity-avatars/avatars/1024/girl-in-ballcap-icon.png'}} style={{
              // marginTop: 30,
              borderRadius: 50,
              // borderColor: '#6FCF97',
              width: 95,
              height: 95,
              // borderWidth: 2,
              // alignSelf: 'center'
            }}
            />

            <Text style={{ fontFamily: Fonts.type.base, fontSize: 18, color: '#fff', paddingTop: 12}}>John Doe</Text>

            <View style={{flexDirection: "row"}}>
              <Text style={{fontFamily: Fonts.type.base, paddingLeft: 5, color: '#fff', fontSize: 13}}>{this.props.logginId}</Text>
            </View>
          </View>
          <List itemDivider={false}>
            <ListItem style={styles.listItem}>
              <Body>
                <Button iconLeft transparent dark full style={{justifyContent: 'flex-start', alignItems: 'center'}} onPress={() => {}}>
                  <Icon name="home"/>
                  <Text style={styles.menuText} uppercase={false}>Home</Text>
                </Button>
              </Body>
            </ListItem>

            <ListItem style={styles.listItem}>
              <Body>
              <Button iconLeft transparent dark full style={{justifyContent: 'flex-start', alignItems: 'center'}} onPress={() => {}}>
                <Icon name="briefcase"/>
                <Text style={styles.menuText} uppercase={false}>Official Accounts</Text>
              </Button>
              </Body>
            </ListItem>

            <ListItem style={styles.listItem}>
              <Body>
              <Button iconLeft transparent dark full style={{justifyContent: 'flex-start', alignItems: 'center'}} onPress={() => {}}>
                <Icon name="md-person-add"/>
                <Text style={styles.menuText} uppercase={false}>Invite Friends</Text>
              </Button>
              </Body>
            </ListItem>

            <ListItem style={styles.listItem}>
              <Body>
              <Button iconLeft transparent dark full style={{justifyContent: 'flex-start', alignItems: 'center'}} onPress={() => {}}>
                <Icon name="md-settings"/>
                <Text style={styles.menuText} uppercase={false}>Settings</Text>
              </Button>
              </Body>
            </ListItem>

            <ListItem style={styles.listItem}>
              <Body>
              <Button iconLeft transparent dark full style={{justifyContent: 'flex-start', alignItems: 'center'}} onPress={() => {}}>
                <Icon name="md-contact"/>
                <Text style={styles.menuText} uppercase={false}>Profile</Text>
              </Button>
              </Body>
            </ListItem>

          </List>
				</Content>
			</Container>
		);
	}
}
const mapStateToProps = (state) => {
  return {
    logginId: state.appCore.loggedInUserId
  }
}

const mapDispatchToProps = (dispatch) => {
  return {


  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DrawerContent)
