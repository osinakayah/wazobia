import React, { PropTypes } from "react";
import {View, AsyncStorage, NetInfo} from "react-native";
import { connect } from "react-redux";
import { Images, Colors , Fonts} from "../Themes";
import LoginActions from "../Redux/LoginRedux";
import { Container, Content, Text, Header, Left, Body, Right, Title, Button, Icon, Toast } from "native-base";
import PhoneInput from 'react-native-phone-input'
import CountryPicker from 'react-native-country-picker-modal';
import styles from './Styles/LoginScreenStyles'
import SpinnerOverlay from 'react-native-loading-spinner-overlay';
import {NavigationActions} from "react-navigation";
import SplashScreen from 'react-native-splash-screen'
class LoginScreen extends React.Component {
  constructor(props){

    super(props);
    this.onPressFlag = this.onPressFlag.bind(this);
    this.selectCountry = this.selectCountry.bind(this);
    this.state = {
      cca2: 'NG',
      countryName: 'Nigeria'
    };

  }

  componentWillReceiveProps = (newProps) => {
    if (newProps.login.smsToken){
      this.props.navigation.navigate('PhoneCodeVerificationScreen', {phoneNumber: this.phone.getValue(), verificationCode: (newProps.login.smsToken).toString()});
    }
  }

  _btnNextPress = () => {
    NetInfo.isConnected.fetch().then(isConnected => {

      if(isConnected){
        if(this.phone.isValidNumber()){
          this.props.loginSetPhoneNumber(this.phone.getValue());
          this.props.attemptLogin(this.phone.getValue());
        }else{
          Toast.show({
            text: 'Invalid Phone Number',
            position: 'top',
            type: 'danger',
            duration: 3000
          })
        }
      }else{
        Toast.show({
          text: 'No Internet connection',
          position: 'top',
          type: 'danger',
          duration: 3000
        })
      }
    });

  }

  componentDidMount() {
    this.phone.focus();
    this.setState({
      pickerData: this.phone.getPickerData(),
    });
    let Contacts = require('react-native-contacts');
    Contacts.checkPermission((err, permission) => {

      if(permission === 'authorized'){

      }
      else if(permission === 'denied' || permission == 'undefined'){
        Contacts.requestPermission( (err, permission) => {
          if(permission === 'authorized'){
          }else{
            alert('Permission to read contacts denied');
          }
        })
      }
    });


  }

  onPressFlag() {
    this.countryPicker.openModal();
  }

  selectCountry(country) {
    this.phone.selectCountry(country.cca2.toLowerCase());
    this.setState({ cca2: country.cca2, countryName: country.name });
  }

  render () {
		return (
      <Container style={{ flex: 1 }}>
        <Header style={{backgroundColor: Colors.primary}} androidStatusBarColor={ Colors.primary_dark}>
          <Left></Left>
          <Body>
            <Title>My Phone Number</Title>
          </Body>
          <Right>
            <Button onPress={this._btnNextPress.bind(this)} transparent><Icon name='md-checkmark' /></Button>
          </Right>
        </Header>
        <Content style={styles.container} padder>
          <SpinnerOverlay visible={this.props.login.fetching} textContent={"Loading..."} textStyle={{color: '#FFF'}} />
          <Text style={{textAlign: 'center', marginTop:15, marginBottom:15, fontFamily: Fonts.type.base}}>Wazobia will send an SMS message to verify your phone number. Enter your country code and phone number:</Text>
          <Button full transparent onPress={this.onPressFlag}><Text style={{textAlign: 'center', marginTop:15, marginBottom:15, fontFamily: Fonts.type.bold}}>{this.state.countryName}</Text></Button>
          <PhoneInput
            style={{marginLeft:30}}
            ref={(ref) => {
              this.phone = ref;
            }}
            onPressFlag={this.onPressFlag}

            initialCountry='ng'/>
          <CountryPicker
            ref={(ref) => {
              this.countryPicker = ref;
            }}
            onChange={value => this.selectCountry(value)}
            translation="eng"
            cca2={this.state.cca2}>
            <View />
          </CountryPicker>

          <Button onPress={this._btnNextPress.bind(this)} block rounded style={{marginTop:70, backgroundColor: Colors.primary}}><Text>Next</Text></Button>
        </Content>
      </Container>
		);
	}
}

const mapStateToProps = state => {
	return {
    login: state.login
	};
};

const mapDispatchToProps = dispatch => {
	return {
	  loginSetPhoneNumber: phoneNumber => dispatch(LoginActions.loginSetPhoneNumber(phoneNumber)),
    loginSetCca: cca2 => dispatch(LoginActions.loginSetCca(cca2)),
    loginSetCountryName: countryName => dispatch(LoginActions.loginSetCountryName(countryName)),
		attemptLogin: phoneNumber => dispatch(LoginActions.loginRequest(phoneNumber))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
