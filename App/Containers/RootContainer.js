import React, { Component } from "react";
import {View, StatusBar, AppState, Platform, AsyncStorage, NetInfo} from "react-native";
import ReduxNavigation from "../Navigation/ReduxNavigation";
import { connect } from "react-redux";
import StartupActions from "../Redux/StartupRedux";
import GroupListActions from '../Redux/GroupListRedux'
import CoreActions from '../Redux/CoreRedux';
import ReduxPersist from "../Config/ReduxPersist";
import FCM, {FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType, NotificationActionType, NotificationActionOption, NotificationCategoryOption} from "react-native-fcm";
import {registerKilledListener, registerAppListener} from "../FirebaseWorkers/Listeners";
import { createDatabase } from "../Databases/InitDB";
import { setValueOnFirebaseDB } from "../FirebaseWorkers/FirebaseDB";
// Styles
import styles from "./Styles/RootContainerStyles";
import {APP_ID} from "../Constants";
import SendBird from "sendbird";
import { Colors } from '../Themes'
var sb = null;
registerKilledListener();
class RootContainer extends Component {
  getFCMToken = () =>{
    try{
      let result = FCM.requestPermissions({badge: false, sound: true, alert: true}).then((response, reject) => {

      });
    } catch(e){
      //console.error(e);
    }

    if(Platform.OS === 'ios'){
      FCM.getAPNSToken().then(token => {

        AsyncStorage.multiSet([
          ['user_fcm_token', token],
        ], ()=>{});
      });
    }else{
      FCM.getFCMToken().then(token => {

        AsyncStorage.multiSet([
          ['user_fcm_token', token],
        ], ()=>{});
      });
    }
  }
  handleFirstConnectivityChange(connectionInfo) {
    //console.log('First change, type: ' + connectionInfo.type + ', effectiveType: ' + connectionInfo.effectiveType);
  }
  _handleAppStateChange = currentAppState => {

    if (currentAppState === 'active') {
      if (sb) {
        sb.setForegroundState();
      }
    } else if (currentAppState === 'background') {
      if (sb) {
        sb.setBackgroundState();
      }
    }
  };

  componentWillUnmount() {
    //NetInfo.isConnected.removeEventListener('change', this.handleFirstConnectivityChange);
    NetInfo.removeEventListener('connectionChange', this.handleFirstConnectivityChange);
    AppState.removeEventListener('change', this._handleAppStateChange);
    if(sb) {
      sb.removeConnectionHandler('Connection_Event');
    }
  }

  componentDidMount() {
    createDatabase();


    this.getFCMToken();

    sb = new SendBird({ appId: APP_ID });
    let ConnectionHandler = new sb.ConnectionHandler();

    ConnectionHandler.onReconnectSucceeded = () => {
      console.log(1)
       this.props.setConnectionStatus(1);
      this.props.getGroupList();
    }
    ConnectionHandler.onReconnectStarted = ()=> {
      this.props.setConnectionStatus(0);
      console.log(2)
    };

    ConnectionHandler.onReconnectFailed = () => {
      this.props.setConnectionStatus(-1);
      // console.log(-1)
      // if(sb){
      //   sb.reconnect();
      // }else{
      //   sb = new SendBird({ appId: APP_ID });
      //   this.props.initSendBird(sb);
      //   this.props.connectToChatSdk();
      //   AsyncStorage.multiGet(['user_phone_number', 'user_fcm_token']).then(data => {
      //
      //     if(data[0][1]){
      //       NetInfo.isConnected.fetch().then(isConnected => {
      //         if(isConnected){
      //           if(data[1][1]){
      //             setValueOnFirebaseDB('users', data[0][1], {fcm_token: data[1][1]})
      //           }
      //           this.props.connectToChatSdk(data[0][1]);
      //         }
      //       });
      //
      //     }
      //   });
      // }
    }
    sb.addConnectionHandler('Connection_Event', ConnectionHandler);

    this.props.initSendBird(sb);
    AsyncStorage.multiGet(['user_phone_number', 'user_fcm_token']).then(data => {

      if(data[0][1]){
        NetInfo.isConnected.fetch().then(isConnected => {
          if(isConnected){
            if(data[1][1]){
              setValueOnFirebaseDB('users', data[0][1], {fcm_token: data[1][1]})
            }
            this.props.connectToChatSdk(data[0][1]);
          }
        });

      }
    });
    NetInfo.addEventListener('connectionChange', this.handleFirstConnectivityChange);
    //NetInfo.isConnected.addEventListener('change', this.handleFirstConnectivityChange);
    AppState.addEventListener('change', this._handleAppStateChange);
    if (!ReduxPersist.active) {
      this.props.startup();
    }
    //this.props.getGroupList();

  }

	render() {
		return (
			<View style={styles.applicationView}>
				<StatusBar barStyle="light-content" backgroundColor='#123aaa' />
				<ReduxNavigation />
			</View>
		);
	}
}

// wraps dispatch to create nicer functions to call within our component
const mapDispatchToProps = dispatch => ({
	startup: () => dispatch(StartupActions.startup()),
  getGroupList: ()=> dispatch(GroupListActions.groupListRequest()),
  initSendBird:(sb)=> dispatch(CoreActions.coreSendBirdInstance(sb)),
  connectToChatSdk: loginId=> dispatch(CoreActions.coreConnectToChatSdk(loginId)),
  setConnectionStatus: status => dispatch(CoreActions.coreConnectToChatSdkSuccess(status))
});

export default connect(null, mapDispatchToProps)(RootContainer);
