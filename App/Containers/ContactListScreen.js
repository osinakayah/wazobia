import React, { Component } from 'react'
import { BackHandler, FlatList } from 'react-native'
import { Content, Container, Header, Left, Right, Body, Button, Text, Title, Icon, Footer, ListItem } from 'native-base'
import { connect } from 'react-redux'
import ContactListActions from '../Redux/ContactListRedux'
import UserAvatar from 'react-native-user-avatar';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/ContactListScreenStyle'

import SpinnerOverlay from 'react-native-loading-spinner-overlay';
import {startNewChat } from "../ChatUtils/StartChat";
import {Colors} from "../Themes";

class ContactListScreen extends Component {

  constructor(props){
    super(props);
  }

  startChat = (name, number) => {
    let userNumber = number.replace("0", "+234");
    startNewChat(name, userNumber, '', '', null, 0, 0);
    this.props.navigation.navigate('SingleChatScreen', {channelUrl: '', userIds: userNumber, chatTitle: name});
  }
  renderPage = () => {
    let {contacts} =  this.props.contactList.contacts;
    return <FlatList
      data={contacts ? contacts.contacts: []}
      keyExtractor={item => item.number}
      renderItem = {({item}) =>
        <ListItem avatar style={{marginTop: 3, marginBottom: 3}} onPress={()=> this.startChat(item.givenName, item.number)}>
          <Left><UserAvatar size="50" name={item.givenName}/></Left>
          <Body>
            <Text>{item.givenName}</Text>
            <Text note>{item.number}</Text>
          </Body>

        </ListItem>
      }
    />
  }
  componentDidMount () {
    this.props.fetchContacts();

    //this.getContactList();
  }
  render () {
    const { contactList } = this.props;
    const {goBack} = this.props.navigation;
    return (
      <Container>
        <Header style={{backgroundColor: Colors.primary}} androidStatusBarColor={ Colors.primary_dark} searchBar={true}>
          <Left>
            <Button transparent onPress={() => {
              goBack();
            }}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Contacts</Title>
          </Body>
          <Right />

        </Header>

        <Content padder>
          <SpinnerOverlay visible={contactList.fetching} textContent={"Loading..."} textStyle={{color: '#FFF'}} />
          {this.renderPage()}
        </Content>

      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    contactList: state.contactList
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchContacts: () => dispatch(ContactListActions.contactListRequest(null))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ContactListScreen)
