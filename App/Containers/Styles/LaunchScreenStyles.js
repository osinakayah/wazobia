import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles } from '../../Themes/'
import Fonts from "../../Themes/Fonts";
import Colors from "../../Themes/Colors";

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  logo: {
    marginTop: Metrics.smallMargin,
    height: Metrics.images.logo,
    width: Metrics.images.logo,
    resizeMode: 'contain'
  },
  centered: {
    paddingTop:50,
    paddingBottom:50,
    alignItems: 'center'
  },
  textInput: {
    height: 40,
    color: Colors.silver,
    borderColor: Colors.accent,
    fontFamily: Fonts.type.base
  }
})
