import { Fonts } from "../../Themes";
export default {
  container: {
    flex: 1,
    padding: 20,
    backgroundColor: '#fff'
  },
  logo: {
    width: 260,
    resizeMode: 'contain'
  },
  listItem: {
    paddingTop: 10,
    paddingBottom: 10,
    marginLeft: 0,
    marginRight: 0,
  },
  menuText: {
    fontSize: 15,
    paddingLeft: 10,
    fontFamily: Fonts.type.base
  },
}
