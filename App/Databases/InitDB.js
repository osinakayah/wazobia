import SQLite from 'react-native-sqlite-storage';
import {GROUP_TABLE, MESSAGES_TABLE} from '../Constants'
export function createDatabase() {
  SQLite.enablePromise(true);
  SQLite.openDatabase("wazobia_chat.db", "1.3", "Wazobia Database", 200000, success, err => {});
}

let success = (db) => {
  let stmt = `CREATE TABLE IF NOT EXISTS ${GROUP_TABLE} (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    group_name TEXT NOT NULL,
    user_id TEXT UNIQUE,
    group_url TEXT,
    display_url TEXT,
    last_message TEXT,
    unread_message_count INTEGER,
    is_sync TINYINT,
    updated_at DATETIME
  )`;

  let stmtCreateMessages = `CREATE TABLE IF NOT EXISTS ${MESSAGES_TABLE}(
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      message_body TEXT NOT NULL,
      created_at DATETIME,
      updated_at DATETIME,
      user TEXT NOT NULL,
      image TEXT,
      audio TEXT,
      video TEXT,
      is_sent TINYINT,
      group_identifier TEXT NOT NULL
  )`;
  if(db){

    db.transaction(tx => {
      tx.executeSql(stmt, [], ()=>{}, ()=>{});
      tx.executeSql(stmtCreateMessages, [], ()=>{}, ()=>{});

      //tx.executeSql('DROP TABLE ' + GROUP_TABLE, [], ()=>{console.log('success table')}, ()=>{console.log('table error')});
      //tx.executeSql('DROP TABLE ' + MESSAGES_TABLE, [], ()=>{console.log('success drop message table')}, ()=>{console.log('table error')});
    })
  }

};

export function insertIntoTableQueryGenerator(tableName, data) {
  let query = 'INSERT OR IGNORE INTO ' + tableName;
  let columns = '(';
  let values = '(';
  let arrayLength = Object.keys(data).length;
  let index = 1;

  for(let prop in data){
    if(index != arrayLength){
      columns = columns + prop +',';
      values = `${values}'${data[prop]}',`;
    }else{
      columns = columns + prop +')';
      values = `${values}'${data[prop]}')`;
    }

    index++;
  }
  return query + columns + ' VALUES ' + values;
}


export function openDatabaseAndPerformOperation(stmt, data) {
  SQLite.openDatabase("wazobia_chat.db", "1.3", "Wazobia Database", 200000, (db) =>{
    if(db){
      db.transaction(tx => {

        tx.executeSql(stmt, [], (tx, result) =>{

        }, (err)=>{});
      })
    }

  }, err => {});
}

export function selectQueryGenerator (tableName, columns, where, orderBy) {
  let query = 'SELECT ';
  if(columns.length === 0){
    query = query + '*';
  }else{
    //Implement later
  }

  query = query+ ' FROM '+ tableName;

  if(where.length !== 0 ){
    //Implement where clause later
  }
  query = query+ ' WHERE last_message != \'null\''
  query = query + ' ORDER BY ' + orderBy[0] + ' ' + orderBy[1];

  return query;

}

export function selectGroupList() {
  return SQLite.openDatabase("wazobia_chat.db", "1.1", "Wazobia Database", 200000);
}

