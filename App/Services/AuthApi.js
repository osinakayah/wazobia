import apisauce from 'apisauce'
import { BASE_URL } from "../Constants";

const api = apisauce.create({baseURL: BASE_URL + 'api'});
export function sendSmsVerification(phoneNumber){
  return api.post('/verify_phone_number', {phoneNumber});
}
