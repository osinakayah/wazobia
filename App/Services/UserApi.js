import apisauce from 'apisauce'
import { BASE_URL, SEND_BIRD_BASE_URL } from "../Constants";

const api = apisauce.create({
  baseURL: SEND_BIRD_BASE_URL,
  headers: {'Api-Token': '2ebf9e58482fd586274893eca793eb30adb3585f'}
});
export function createUser(user){
  return api.post('/users', user);
}
