 import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import styles from './Styles/NewsListStyle'
import {Image} from 'react-native'
import { Images, Colors , Fonts, Metrics} from "../Themes";
import { Container, Header, View, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body } from 'native-base';
 import {
   Content,
   Title,
   Right,
 } from "native-base";

export default class NewsList extends Component {
  // // Prop type warnings
  // static propTypes = {
  //   someProperty: PropTypes.object,
  //   someSetting: PropTypes.bool.isRequired,
  // }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  render () {
    return (
        <Content style={styles.container} padder>
          <Card style={{flex: 0}}>
            <CardItem>
              <Left>
                <Thumbnail source={{uri: 'http://icons.iconarchive.com/icons/diversity-avatars/avatars/1024/girl-in-ballcap-icon.png'}} />
                <Body>
                <Text>NativeBase</Text>
                <Text note>April 15, 2018</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem>
              <Body>
              <Image source={{uri: 'http://icons.iconarchive.com/icons/diversity-avatars/avatars/1024/girl-in-ballcap-icon.png'}} style={{height: 200, width: Metrics.screenWidth, flex: 1}}/>
              <Text>
                Cat, and vanished. Alice was thoroughly puzzled. 'Does the boots and shoes!' she repeated in a hoarse growl, 'the world would go round and round goes the clock in a trembling voice to its children.
              </Text>
              </Body>
            </CardItem>
            <CardItem>
              <Left>
                <Button transparent textStyle={{color: '#87838B'}}>
                  <Icon name="logo-github" />
                  <Text>1,926 stars</Text>
                </Button>
              </Left>
            </CardItem>
          </Card>

          <Card style={{flex: 0}}>
            <CardItem>
              <Left>
                <Thumbnail source={{uri: 'http://icons.iconarchive.com/icons/diversity-avatars/avatars/1024/girl-in-ballcap-icon.png'}} />
                <Body>
                <Text>NativeBase</Text>
                <Text note>April 15, 2018</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem>
              <Body>
              <Image source={{uri: 'http://icons.iconarchive.com/icons/diversity-avatars/avatars/1024/girl-in-ballcap-icon.png'}} style={{height: 200, width: Metrics.screenWidth, flex: 1}}/>
              <Text>
                Cat, and vanished. Alice was thoroughly puzzled. 'Does the boots and shoes!' she repeated in a hoarse growl, 'the world would go round and round goes the clock in a trembling voice to its children.
              </Text>
              </Body>
            </CardItem>
            <CardItem>
              <Left>
                <Button transparent textStyle={{color: '#87838B'}}>
                  <Icon name="logo-github" />
                  <Text>1,926 stars</Text>
                </Button>
              </Left>
            </CardItem>
          </Card>

          <Card style={{flex: 0}}>
            <CardItem>
              <Left>
                <Thumbnail source={{uri: 'http://icons.iconarchive.com/icons/diversity-avatars/avatars/1024/girl-in-ballcap-icon.png'}} />
                <Body>
                <Text>NativeBase</Text>
                <Text note>April 15, 2018</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem>
              <Body>
              <Image source={{uri: 'http://icons.iconarchive.com/icons/diversity-avatars/avatars/1024/girl-in-ballcap-icon.png'}} style={{height: 200, width: Metrics.screenWidth, flex: 1}}/>
              <Text>
                Cat, and vanished. Alice was thoroughly puzzled. 'Does the boots and shoes!' she repeated in a hoarse growl, 'the world would go round and round goes the clock in a trembling voice to its children.
              </Text>
              </Body>
            </CardItem>
            <CardItem>
              <Left>
                <Button transparent textStyle={{color: '#87838B'}}>
                  <Icon name="logo-github" />
                  <Text>1,926 stars</Text>
                </Button>
              </Left>
            </CardItem>
          </Card>

          <Card style={{flex: 0}}>
            <CardItem>
              <Left>
                <Thumbnail source={{uri: 'http://icons.iconarchive.com/icons/diversity-avatars/avatars/1024/girl-in-ballcap-icon.png'}} />
                <Body>
                <Text>NativeBase</Text>
                <Text note>April 15, 2018</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem>
              <Body>
              <Image source={{uri: 'http://icons.iconarchive.com/icons/diversity-avatars/avatars/1024/girl-in-ballcap-icon.png'}} style={{height: 200, width: Metrics.screenWidth, flex: 1}}/>
              <Text>
                Cat, and vanished. Alice was thoroughly puzzled. 'Does the boots and shoes!' she repeated in a hoarse growl, 'the world would go round and round goes the clock in a trembling voice to its children.
              </Text>
              </Body>
            </CardItem>
            <CardItem>
              <Left>
                <Button transparent textStyle={{color: '#87838B'}}>
                  <Icon name="logo-github" />
                  <Text>1,926 stars</Text>
                </Button>
              </Left>
            </CardItem>
          </Card>
        </Content>
    )
  }
}
