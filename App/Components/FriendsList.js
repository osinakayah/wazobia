import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { View, Text, List, ListItem, Left, Body , Right, Fab,Button, Icon, Grid, Row } from 'native-base'
import styles from './Styles/FriendsListStyle'
import { Colors, Metrics, Images } from '../Themes'
import UserAvatar from 'react-native-user-avatar';
import {AsyncStorage, FlatList, Image, Platform} from "react-native";

import SpinnerOverlay from 'react-native-loading-spinner-overlay';

var sb = null;

export default class FriendsList extends Component {

  constructor(props){
    super(props);
  }

  formatDate(dateInMilliseconds){
    let date = new Date(dateInMilliseconds);
    return date.getHours()+':'+ date.getMinutes();
  }
  renderPage = () => {

    return <FlatList
      data={this.props.groupList}
      keyExtractor={item => item.id}
      renderItem = {({item}) =>
        <ListItem avatar onPress={()=>this.props.openSingleChat(item.group_url, item.user_id, item.group_name)}>
          <Left><UserAvatar size="50" name={item.group_name}/></Left>
          <Body>
            <Text>{item.group_name}</Text>
            <Text note>{item.last_message}</Text>
          </Body>
          <Right>
            <Text note>{this.formatDate(item.updated_at)}</Text>
          </Right>
        </ListItem>
      }
    />
  }
  render () {
    return (
      <View style={{ display:'flex', flex: 1}}>
        <SpinnerOverlay visible={this.props.isConnected == 0} textContent={"Connecting..."} textStyle={{color: '#FFF'}} />
        {(this.props.isConnected == -1) ? <Grid style={{display:'flex',  justifyContent:'center', alignItems:'center', marginTop: (Metrics.screenHeight/4) }}>
          <Row style={{height: 70, width: 70, marginBottom: 30}}><Image style={{height: 70, width: 70}} source={Images.no_internet}/></Row>
          <Row><Button block rounded style={{backgroundColor: Colors.primary, marginLeft: 120, marginRight: 120}} onPress={()=>{this.props.retryConnection()}}><Text>Retry</Text></Button></Row>
          </Grid> : this.renderPage()}

        <Fab
          direction="up"
          containerStyle={{ }}
          style={{ backgroundColor: Colors.accent_color }}
          position="bottomRight"
          onPress={() => {this.props.openContactsList()}}>
          <Icon name="md-add" />
        </Fab>
      </View>
    )
  }
}
