
export const APP_ID = 'D76A8C0E-0CA3-485E-8A7D-3CD446CD98DE';
export const BASE_URL = 'http://127.0.0.1:8000/';
export const SEND_BIRD_BASE_URL = 'https://api.sendbird.com/v3';
export const DEFAULT_USER_PROFILE_PHOTO = 'http://icons.iconarchive.com/icons/diversity-avatars/avatars/1024/girl-in-ballcap-icon.png';
export const GROUP_TABLE = 'groups';
export const MESSAGES_TABLE = 'messages';
