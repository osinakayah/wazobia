import React from "react";
import { StackNavigator } from "react-navigation";
import styles from "./Styles/NavigationStyles";

// screens identified by the router
import Login from "../Containers/LoginScreen";
import LaunchScreen from "../Containers/LaunchScreen";
import NavigationDrawer from "./NavigationDrawer";
import PhoneCodeVerificationScreen from "../Containers/PhoneCodeVerificationScreen";
import SingleChatScreen from "../Containers/SingleChatScreen";
import ContactListScreen from "../Containers/ContactListScreen";
import IntroScreen from "../Containers/IntroScreen";

const PrimaryNav = StackNavigator(
	{
    IntroScreen: { screen: IntroScreen },
    ContactListScreen: { screen: ContactListScreen },
    SingleChatScreen: { screen: SingleChatScreen },
    PhoneCodeVerificationScreen: { screen: PhoneCodeVerificationScreen },
		Login: { screen: Login },
		LaunchScreen: { screen: LaunchScreen },
		NavigationDrawer: { screen: NavigationDrawer },
	},
	{
		initialRouteName: 'IntroScreen',
		headerMode: "none",
	}
);

export default PrimaryNav;
