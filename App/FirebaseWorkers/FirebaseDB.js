import * as firebase from "firebase";
let fireBaseDB = null;
export function   setValueOnFirebaseDB(path, id, data){
  let config = {
    apiKey: "AIzaSyDYI2hQFTEFw8jUv0BlVM9Q8v4aH0gLy0w",
    authDomain: "wazobia-200317.firebaseapp.com",
    databaseURL: "https://wazobia-200317.firebaseio.com",
    projectId: "wazobia-200317",
    storageBucket: "wazobia-200317.appspot.com",
    messagingSenderId: "147389723830"
  };

  if(fireBaseDB == null){
    let firebaseApp = firebase.initializeApp(config);
    fireBaseDB = firebaseApp.database();
  }

  fireBaseDB.ref(path + '/' + id).set(data)
}

export function getFCMDBReference() {
  if(fireBaseDB != null){
    return fireBaseDB;
  }
  let config = {
    apiKey: "AIzaSyDYI2hQFTEFw8jUv0BlVM9Q8v4aH0gLy0w",
    authDomain: "wazobia-200317.firebaseapp.com",
    databaseURL: "https://wazobia-200317.firebaseio.com",
    projectId: "wazobia-200317",
    storageBucket: "wazobia-200317.appspot.com",
    messagingSenderId: "147389723830"
  };
  let firebaseApp = firebase.initializeApp(config);
  fireBaseDB = firebaseApp.database();
  return fireBaseDB;

}
