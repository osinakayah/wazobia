import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  loginRequest: ['phoneNumber'],
  loginSetPhoneNumber: ['phoneNumber'],
  loginSetCountryName: ['countryName'],
  loginSetCca: ['cca2'],
  loginSuccess: ['smsToken'],
  loginFailure: ['error'],
  logout: null
})

export const LoginTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  phoneNumber: null,
  error: null,
  fetching: false,
  smsToken: null,
  cca2: null,
  countryName: null
})

/* ------------- Reducers ------------- */
export const phoneNumber = (state, { phoneNumber }) =>
  state.merge({ phoneNumber })

export const cca2 = (state, { cca2 }) =>
  state.merge({ cca2 })

export const countryName = (state, { countryName }) =>
  state.merge({ countryName })

// we're attempting to login
export const request = (state) => state.merge({ fetching: true, smsToken: null })

// we've successfully logged in
export const success = (state, { smsToken }) =>
  state.merge({ fetching: false, error: null, smsToken })

// we've had a problem logging in
export const failure = (state, { error }) =>
  state.merge({ fetching: false, error, smsToken: null })

// we've logged out
export const logout = (state) => INITIAL_STATE

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.LOGIN_REQUEST]: request,
  [Types.LOGIN_SUCCESS]: success,
  [Types.LOGIN_FAILURE]: failure,
  [Types.LOGOUT]: logout,
  [Types.LOGIN_SET_PHONE_NUMBER]: phoneNumber,
  [Types.LOGIN_SET_COUNTRY_NAME]: countryName,
  [Types.LOGIN_SET_CCA]: cca2
})

/* ------------- Selectors ------------- */

// Is the current user logged in?
export const isLoggedIn = (loginState) => loginState.username !== null
