import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  homeRequestGroupList: ['dataGroupList'],
  homeSuccessGroupList: ['payloadGroupList'],
  homeFailureGroupList: null
})

export const HomeTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  dataGroupList: null,
  fetchingGroupList: null,
  payloadGroupList: null,
  groupList: [],
  errorGroupList: null
})

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state) =>
  state.merge({ fetching: true })

// successful api lookup
export const success = (state, groupList) => {
  const { payloadGroupList } = groupList
  return state.merge({ fetching: false, error: null, groupList: payloadGroupList })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, groupList: [] })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.HOME_REQUEST_GROUP_LIST]: request,
  [Types.HOME_SUCCESS_GROUP_LIST]: success,
  [Types.HOME_FAILURE_GROUP_LIST]: failure
})
