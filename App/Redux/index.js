import { combineReducers } from 'redux'
import configureStore from './CreateStore'
import rootSaga from '../Sagas/'

export default () => {
  /* ------------- Assemble The Reducers ------------- */
  const rootReducer = combineReducers({
    nav: require('./NavigationRedux').reducer,
    github: require('./GithubRedux').reducer,
    login: require('./LoginRedux').reducer,
    search: require('./SearchRedux').reducer,
    phoneCodeVerificationTypes: require('./PhoneCodeVerificationRedux').reducer,
    contactList: require('./ContactListRedux').reducer,
    groupList: require('./GroupListRedux').reducer,
    appCore: require('./CoreRedux').reducer,
    homeStore: require('./HomeRedux').reducer,
    singleChatRedux: require('./SingleChatRedux').reducer
  })

  return configureStore(rootReducer, rootSaga)
}
