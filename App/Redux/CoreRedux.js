import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  coreConnectToChatSdk: ['data'],
  coreConnectToChatSdkSuccess: ['isConnected'],
  coreConnectToChatSdkFailure: ['isConnected'],
  coreRequest: ['data'],
  coreSuccess: ['payload'],
  coreFailure: null,
  coreLoggedInUserId: ['loggedInUserId'],
  coreSendBirdInstance: ['sendBirdInstance'],
  coreFcmTokenRefreshed: ['fcmToken']
})

export const CoreTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null,
  isConnected: 0,
  loggedInUserId: 0,
  sendBirdInstance: null
})

/* ------------- Selectors ------------- */

export const CoreSelectors = {
  getData: state => state.appCore.isConnected
}
export const CoreSelectorsAllData = {
  getData: state => state.appCore
}

/* ------------- Reducers ------------- */

export const coreSendBirdInstance = (state, sendBirdInstance) => state.merge({ sendBirdInstance })

export const connectToSdk = (state, { data }) =>
  state.merge({ data });

export const coreLoggedInUserId = (state, { loggedInUserId }) => {
  return state.merge({loggedInUserId});
}


export const isConnected = (state, { isConnected }) =>
  state.merge( { isConnected })

// request the data from an api
export const request = (state, { data }) =>
  state.merge({ fetching: true, data, payload: null })

// successful api lookup
export const success = (state, action) => {
  const { payload } = action
  return state.merge({ fetching: false, error: null, payload })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null })

export const fcmTokenRefreshed = state => state;

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.CORE_REQUEST]: request,
  [Types.CORE_SUCCESS]: success,
  [Types.CORE_FAILURE]: failure,
  [Types.CORE_CONNECT_TO_CHAT_SDK]: connectToSdk,
  [Types.CORE_CONNECT_TO_CHAT_SDK_SUCCESS]: isConnected,
  [Types.CORE_CONNECT_TO_CHAT_SDK_FAILURE]: isConnected,
  [Types.CORE_LOGGED_IN_USER_ID]: coreLoggedInUserId,
  [Types.CORE_SEND_BIRD_INSTANCE]: coreSendBirdInstance,
  [Types.CORE_FCM_TOKEN_REFRESHED]: fcmTokenRefreshed
})
