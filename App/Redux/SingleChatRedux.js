import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  singleChatRequestCreateGroupChannel: ['data'],
  singleChatSuccessCreateGroupChannel: ['groupChannel'],
  singleChatFailureCreateGroupChannel: null,
  singleChatSendMessage: ['userId', 'message', 'fromUserId', 'toUserId'],
  singleChatFetchChannelMessages: ['otherPartyId'],
  singleChatSetMessages: ['messages'],
  singleChatSetCustomText: ['customText']
})

export const SingleChatTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null,
  groupChannel: null,
  messages: [],
  customText: ''
})

export const SingleChatSelectors = {
  getData: state => state.singleChatRedux
}

/* ------------- Reducers ------------- */
export const setCustomText = (state, {customText}) => state.merge({customText})
export const setMessages = (state, {messages}) => state.merge({messages})

export const fetchChannelMessages = state => state

// request the data from an api
export const request = (state, { data }) =>
  state.merge({ fetching: true, data, payload: null })

// successful api lookup
export const success = (state, action) => {
  const { groupChannel } = action
  return state.merge({ fetching: false, error: null, groupChannel })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null })

export const sendMessage = state => state;

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SINGLE_CHAT_REQUEST_CREATE_GROUP_CHANNEL]: request,
  [Types.SINGLE_CHAT_SUCCESS_CREATE_GROUP_CHANNEL]: success,
  [Types.SINGLE_CHAT_FAILURE_CREATE_GROUP_CHANNEL]: failure,
  [Types.SINGLE_CHAT_SEND_MESSAGE]: sendMessage,
  [Types.SINGLE_CHAT_FETCH_CHANNEL_MESSAGES]: fetchChannelMessages,
  [Types.SINGLE_CHAT_SET_MESSAGES]: setMessages,
  [Types.SINGLE_CHAT_SET_CUSTOM_TEXT]: setCustomText
})
